<h1>Potimeline</h1>

Work in progress ;)

<h2>Getting started</h2>

The optimal HTML structure is:
	
	<div id="yourTimelineID"></div>


Initialize it with:

	<script type="text/javascript">
		var myTimeline = new Potimeline(document.getElementById("yourTimelineID"), yourJson, options);
	</script>
	 
Json example:

	{
		"ranges" : [2001, 2009, 2016],
		"elements": [
			{
				"thumb": "http://placehold.it/350x150/ff0000",
				"date": "2016-11-22",
				"url": "http://www.google.it",
				"attributes": {
					"data-slug": "google",
					...
				}
			},
			...
		]				
	}
	
Options is optional. You can set this options:

	{
		init: function //the callback function triggered at the end of init
		selected: function //the callback function triggered at the end of each result generation
		defaultImage: string //url or base64 of the broken image placeholder
		htmlListLi: string //html structure of each result (must contains {{url}}, {{thumb}} and can contains {{attributes}})
		urlPrefix: string //prefixed at each url
			
	}


<h2 id="license">License (MIT)</h2>

Copyright (c) 2016 Sara Potyscki, [thepot.site](http://www.thepot.site/portfolio/potimeline)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
